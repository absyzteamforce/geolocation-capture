#**What is this repository for?**#

The inline vf is used in contact record detail page to display the location of the contact.

#**How do I get set up?**#

To set up the functionality of the vf page, 
1. Deploy the field and the page into the org where you would like the functionality to be used in.
2. Edit Page the required page layout in which you want the functionality to be achieved. 
 2.1. Under visual force page drag down the visual force page 'ContactGeoCoder'.
 2.2. Under fields, drag down Location__c.
3. Save the page layout. 

#**How it works.**#

When a user/rep visits a contact, he/she needs to open the contact record detail page, here they can see the vf page with a button on it 'Get Location'. Upon clicking of the button, the location__c field will be populated and the vf page will display the location of the contact where you have tagged.  
This functionality works even in mobile devices.(Salesforce1 Mobile app).

#**Who do I talk to?**#

alexander.charles@absyz.com